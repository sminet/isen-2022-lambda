package fr.yncrea.cin3.geometry;

public interface Surfaceable {
    float getSurface();
}
