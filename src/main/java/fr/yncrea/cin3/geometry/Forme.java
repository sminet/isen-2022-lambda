package fr.yncrea.cin3.geometry;

public abstract class Forme implements Surfaceable {
	protected String description;
	
    @Override
    public String toString() {
        return this.description;
    }
}
