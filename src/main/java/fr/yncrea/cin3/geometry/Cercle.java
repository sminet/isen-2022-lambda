package fr.yncrea.cin3.geometry;

public class Cercle extends Forme {
	private final float rayon;

	public Cercle(float rayon) {
		this.rayon = rayon;
		this.description = "Cercle{" + "r=" + rayon + '}';
	}

	@Override
	public float getSurface() {
		return (float) Math.PI * rayon * rayon;
	}
}
