package fr.yncrea.cin3.geometry;

public class Carre extends Rectangle {
	public Carre(float cote) {
		super(cote, cote);
		this.description = "Carre{" + "c=" + cote + '}';
	}
}
