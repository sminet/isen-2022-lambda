package fr.yncrea.cin3.geometry;

public class Trapeze extends Forme {
	private final float b, B, h;

	public Trapeze(float b, float B, float h) {
		this.b = b;
		this.B = B;
		this.h = h;
		this.description = "Trapèze{" + "b=" + b + ", B=" + B + ", h=" + h + '}';
	}

	@Override
	public float getSurface() {
		return h * (b + B) / 2;
	}
}
