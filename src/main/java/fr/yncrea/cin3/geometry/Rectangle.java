package fr.yncrea.cin3.geometry;

public class Rectangle extends Trapeze {
	public Rectangle(float x, float y) {
		super(x, x, y);
		this.description = "Rectangle{" + "l=" + x + ", h=" + y + '}';
	}
}
