import fr.yncrea.cin3.geometry.*;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Surfaceable> surfaces = new ArrayList<>();
        surfaces.add(new Carre(5));
        surfaces.add(new Rectangle(2, 4));
        surfaces.add(new Trapeze(4, 5, 2));
        surfaces.add(new Cercle(1));

        for (Surfaceable surface: surfaces) {
            System.out.printf("%s possède une surface de %.2f\n", surface.toString(), surface.getSurface());
        }
    }
}